

messageData = []
var print = document.getElementById('print');
var erase = false;
var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

localStorage.setItem('themes', JSON.stringify(themes));
//localStorage.setItem('selected', JSON.stringify(selected));
// Retrieve the object from storage
var retrievedObject = localStorage.getItem('themes');
var retrievedTheme = localStorage.getItem('newSelected');

//console.log('retrievedObject: ', JSON.parse(retrievedObject));
//console.log('retrievedTheme: ', JSON.parse(retrievedTheme));



var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
   	  print.value = '';
   	  erase = true;

  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

function enterMessage(){	
  var key = window.event.keyCode;
  if(key==13){

  	var textArea = document.getElementById('messagenya');
    var isiMessage = document.getElementById('messagenya').value;

    var old=$(".msg-insert").html();
    messageData.push(isiMessage);

    for(var i=0; i<messageData.length; i++){
    	$(".msg-insert").html(old + "<p class='msg-send'>" + messageData[i] + "</p>");
    }
    textArea.value = "";
    $("#messagenya").attr('placeholder',"Press Enter");
    return false;

  }
  return true;

}

$(document).ready(function() {
    $('.my-select').select2();
     $("#arrow").click(function() {
        $(".chat-body").slideToggle();
    });
    $('#selector').select2({
        'data' : JSON.parse(retrievedObject)
    });
    var retrievedTheme = localStorage.getItem('newSelected');
    var appliedTheme = JSON.parse(retrievedTheme);
    $('body').css('background', appliedTheme.selected.bcgColor);
    $('body').css('color', appliedTheme.selected.fontColor);




});

$('.apply').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var id = $(".my-select").select2("val");
    //console.log(id);

    data = $.parseJSON(retrievedObject);
    $.each(data, function(i, item) {
    //console.log(item.id)
    if(id == item.id){
        var text = item.text;
        var background = item.bcgColor;
        var font = item.fontColor;

        $('body').css("background-color",background);
        $('body').css("color", font);

        var tes = {"selected":{"bcgColor":background,"fontColor":font, "text":text}};
        //console.log(tes);
        localStorage.setItem('newSelected', JSON.stringify(tes));
        }
    });
//localStorage.setItem('selected', JSON.stringify(selected));
var retrievedTheme = localStorage.getItem('newSelected');

//console.log(appliedTheme);
var appliedTheme = JSON.parse(retrievedTheme);
console.log(appliedTheme);
});



