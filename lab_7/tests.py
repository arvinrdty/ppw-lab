from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, csui_helper, CSUIhelper
from .models import Friend




class Lab7UnitTest(TestCase):

	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)
	
	def test_lab7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)
	
	def test_friend_list_url_is_exist(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertEqual(response.status_code, 200)
	
	def test_get_friend_list_data_url_is_exist(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)
	
	def test_auth_param_dict(self):
		csui_helper = CSUIhelper()
		auth_param = csui_helper.instance.get_auth_param_dict()
		self.assertEqual(auth_param['client_id'], csui_helper.instance.get_auth_param_dict()['client_id'])
	
	def test_add_friend(self):
		response_post = Client().post(
			'/lab-7/add-friend/', 
			{'name':"kak pewe", 'npm':"8888888888"}
		)
		self.assertEqual(response_post.status_code, 200)




